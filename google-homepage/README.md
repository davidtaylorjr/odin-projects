# google-homepage
A project from The Odin Project and is a work in progress.

http://www.theodinproject.com/web-development-101/html-css?ref=lnav

The goal of this project is to duplicate the basic layout of the homepage at www.google.com. It is not the object of this project to duplicate the functionality of the site.
